#!/bin/bash
set -x
arduino-cli board listall
arduino-cli sketch new accespoint
cp -v accespoint.ino accespoint/
arduino-cli compile --fqbn esp8266:esp8266:nodemcu accespoint
arduino-cli upload -p /dev/ttyUSB0 --fqbn esp8266:esp8266:nodemcu accespoint
